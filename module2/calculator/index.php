<!DOCTYPE html>
    <head>
        <title>Calculator</title>
        <link rel="stylesheet" type="text/css" href="calculator.css"/>
    </head>
    <body>
        <form method="POST" action="results.php">
            <div class="calc">
                <div class="inline position_ops">
                    <input type="radio" name="op" value="plus">Add <br>
                    <input type="radio" name="op" value="minus">Subtract <br>
                    <input type="radio" name="op" value="multiply"> Multiply <br>
                    <input type="radio" name="op" value="divide"> Divide <br>
                </div>
                <div class="inline position_body">
                    <div class="inline">
                        <input type="number" name="first" value="0">
                    </div>
                    <div class="inline">
                        <p>and</p>
                    </div>
                    <div class="inline">
                        <input type="number" name="second" value="0">
                        <input type="submit" value="Send">
                    </div>     
                </div>
            </div>
            
        </form>
    </body>
</html>
    