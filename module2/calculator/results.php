<!DOCTYPE html>
	<head>
		<title>Calculator</title>
	</head>
	<body>

		<div class="response">
	        <?php

	                    if(isset($_POST['op'])){

	                        $op = $_POST['op'];
	                        $first = (float) $_POST['first'];
	                        $second = (float) $_POST['second'];
	                        $result = (float) 0;
	                        if(! $first ){ $first = 0;}
	                        if(! $second ) { $second = 0; }
	                    
	                        switch($op) {

	                            case "plus":
	                                $result = $first + $second;
	                            break;
	                            case "minus":
	                                $result = $first - $second;
	                            break;
	                            case "multiply":
	                                $result = $first * $second;
	                            break;
	                            case "divide":
	                                $result = $first / $second;
	                            
	                        }

	                        echo $result . " is the result of the " . $op . " operation";    
	                    } else {

	                       echo "Please go back and select an operation.";
	                    
	                    }
	            ?>
				<form method="POST" action="index.php">
	            	<input type="submit" value="Back">
				</form>	
	    </div>
	</body>
</html>