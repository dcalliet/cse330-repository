import sys, os, re

#Class handles collection of player objects
class StatCollection:

	def __init__(self):
		self.players = {}

	def addLine(self, string):
		player_data = parse_statistic(string)

		if player_data['name'] in self.players:
			player_obj = self.players[player_data['name']]
			player_obj.addBats(player_data['bats'])
			player_obj.addHits(player_data['hits'])
			player_obj.addRuns(player_data['runs'])
		else:
			self.players[player_data['name']] = Player(player_data)

	def getAvg(self):
		
		player_dicts = []
		for name in self.players:
			player = self.players[name]
			player_dicts.append( (player.getName(), player.getAverage()) )

		return player_dicts

#Class handles data of individual player
class Player:

	def __init__(self, obj):
		self.name = obj['name']
		self.bats = float(obj['bats'])
		self.hits = float(obj['hits'])
		self.runs = float(obj['runs'])

	def addHits(self, num):
		self.hits += num

	def getHits(self):
		return self.hits

	def addBats(self, num):
		self.bats += num

	def getBats(self):
		return self.bats

	def addRuns(self, num):
		self.runs += num

	def getRuns(self):
		return self.bats

	def getName(self):
		return self.name

	def getAverage(self):
		return round( self.hits / self.bats ,3)

def parse_statistic(string):
	string_data = string.split()
	full_name = string_data[0] + " " + string_data[1]
	bats = float(string_data[3])
	hits = float(string_data[6])
	runs = float(string_data[9])

	stat_obj = {'name': full_name, 'bats': bats, 'hits': hits, 'runs': runs}
	return stat_obj


if len(sys.argv) < 2:
	sys.exit("Usage: %s <filename>" % sys.argv[0])

if len(sys.argv) > 2:
	print " << Extra arguments are ignored >> \n"

filename = sys.argv[1]

if not os.path.exists(filename):
	sys.exit("Error: file '%s' not found" % sys.argv[1])

collection = StatCollection()
data_array = []
string_regex = re.compile(r"\w*\s\w*\s*batted\s*\d*\s*times\s*with\s*\d*\s*hits\s*and\s*\d*\s*runs")
f = open(filename)
for line in f:
	data_array.append(line.rstrip())
f.close()

for string in data_array:
	if string_regex.match(string):
		#do some handling of data
		collection.addLine(string)

average_dict = collection.getAvg()
sorted_list = sorted(average_dict, key=lambda players: players[1], reverse=True)

for i in sorted_list:
	print i[0] + ": " + str(i[1])






