Match all strings that contain at least one “hello world”
\b.*hello world.*\b

Match all words with a triple vowel
\w*[aeiou]{3}\w*

Match all flight patterns of the form AA###
\bAA\d{3,4}\b
